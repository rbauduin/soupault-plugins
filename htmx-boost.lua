-- Boost your site with htmx: https://htmx.org/attributes/hx-boost/
-- Example usage in soupault.toml
-- [widgets.htmx-boost]
--  widget = "htmx-boost"
--  # no selector applies the boost to <body>
--  #selector = "a.nav-link"
--  # default loads from cdn with integrity check
--  #htmx_script_tag = '<script src="https://unpkg.com/htmx.org@1.9.8" integrity="sha384-EAzY246d6BpbWR7sQ8+WEm40J8c3dHFsqC58IgPlh4kMbRRI6P6WA+LA/qGAyAu8" crossorigin="anonymous"></script>'

-- determine which elements will be boosted, according to the selector config.
selector = config["selector"]
if not selector then
  selector = "body"
  Log.warning("htmx boost selector is not configured, using default " .. selector)
end


-- Elements to be boosted according to selector
to_boost = HTML.select(page, selector)
to_boost_count = size(to_boost)

-- Elements already boosted in HTML we work on
boosted = HTML.select(page, "*[hx-boost=\"true\"]")
boosted_count = size(boosted)

-- function to determine if the page already has htmx loaded
function has_htmx()
  local htmx = HTML.select(page, "script[src*=\"htmx.org\"]")
  return  size(htmx)>0
end

-- function to add the htmx script element to the page's head
function load_htmx ()
  if not (has_htmx()) then
    head = HTML.select_one(page, "head")
    if head then
      -- build script tag
      htmx_script_tag = config["htmx_script_tag"]
      if not htmx_script_tag then
        Log.warning("htmx is loaded from CDN, it is preferred to be vendored and served locally using option htmx_script_tag.")
        htmx_script= HTML.parse([[<script src="https://unpkg.com/htmx.org@1.9.8" integrity="sha384-EAzY246d6BpbWR7sQ8+WEm40J8c3dHFsqC58IgPlh4kMbRRI6P6WA+LA/qGAyAu8" crossorigin="anonymous"></script>]])
      else
        htmx_script= HTML.parse(htmx_script_tag)
      end
      Log.info("Inserting htmx script node in head")
      HTML.append_child(head, htmx_script)
    else
      Log.error("no head found in document")
    end
  end
end

-- Load htmx if needed
if to_boost_count > 0  or boosted_count > 0 then
  load_htmx()
end

-- Add hx-boost to elements matching the selector
local i = 1
while i <= to_boost_count do
  HTML.set_attribute(to_boost[i],"hx-boost","true")
  i = i+1
end
