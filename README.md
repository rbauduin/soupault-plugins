These are some [Soupault](https://soupault.app/) plugins I developed.
See [this blog post](posts/supercharging_static_site_with_htmx_soupault/) for how to use these with Soupault.
